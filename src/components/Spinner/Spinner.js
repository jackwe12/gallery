import React from 'react';
import './Spinner.css';
const Loader = () => (
    <div className="spinner">Loading...</div>
);

export default Loader;
